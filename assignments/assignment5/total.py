import os

# get all time card files in directory
time_card_files = os.listdir('.')
list_of_time_cards = []
# loop through files
for i in range(len(time_card_files)):
    if time_card_files[i].endswith('.txt'):
        list_of_time_cards.append(time_card_files[i])

# open report for writing
with open('report.yeet', 'w') as report:

    for files in list_of_time_cards:
        listofmins = []
        listofhours = []

        with open(files, "r") as iterator:

            #get hr and minutes
            for line in iterator.readlines()[1:]:
               values = line.split()
               day = values[0]
               hr = values[1]
               minutes = values[2]
               listofmins.append(int(minutes.strip('min')))
               listofhours.append(int(hr.strip('hr')))
               print listofmins
               print listofhours
               # get totals
               sumtotalmin = sum(listofmins)
               sumtotalhr = sum(listofhours)

               print sumtotalmin
        #get name
        with open(files, "r") as iterator2:
            all_text = iterator2.read()
            lines = all_text.split('\n')
            lines.remove('')
            name = lines[0]

        # perform minute to hour calculations
        minutes_per_hour = 60
        total_hours = sumtotalmin // minutes_per_hour
        final_total_hours = total_hours + sumtotalhr
        print total_hours
        remainder = sumtotalmin % minutes_per_hour
        # write to file and format
        report.write('{} {}h {}m\n'.format(name, final_total_hours, remainder))

