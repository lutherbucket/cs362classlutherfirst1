#!/bin/bash

yourfilename='./*/*.tex'
echo LETS GET RECURSIVE YO

for f in $yourfilename; do
    echo ______________ Beginning of $f ___________ >> LatexCompileReport.txt
    echo file name: >> LatexCompileReport.txt
    echo $f >> LatexCompileReport.txt
    echo =========================================== >> LatexCompileReport.txt
    echo word count number of lines: >> LatexCompileReport.txt
    wc -l $f >> LatexCompileReport.txt
    echo =========================================== >> LatexCompileReport.txt
    echo word count number of words: >> LatexCompileReport.txt
    wc -w $f >> LatexCompileReport.txt
    echo =========================================== >> LatexCompileReport.txt
    echo word count number of bytes: >> LatexCompileReport.txt
    wc -c $f >> LatexCompileReport.txt
    echo =========================================== >> LatexCompileReport.txt
    echo -e "______________ End of $f __________________\n \n \n \n" >> LatexCompileReport.txt
    pdflatex $f
    mv *.pdf ./nested_dir
done

echo removing .log .aux .bib and .out files
ls *.log
ls *.aux
ls *.bib
ls *.out
echo =========================================
rm *.log
rm *.aux
rm *.bib
rm *.out
echo all the trash has been removed, sick!
echo =========================================
echo *DING* The scripts result is fresh out of the over, like fresh biscuits
